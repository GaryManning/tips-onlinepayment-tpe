function setupPayPalButton(serverUrl, guid) {
paypal.Button.render({
    
        env: 'sandbox', // Optional: specify 'sandbox' environment

		locale: 'en_GB',

		style: {
			color: 'gold',
			shape: 'rect',
			size: 'tiny'
		},
    
        payment: function(resolve, reject) {
               
            var CREATE_PAYMENT_URL = serverUrl + "initiatepayment";
                
            paypal.request.post(CREATE_PAYMENT_URL, { id: guid })
                .then(function(data) { 
					resolve(data.paymentID);
			   	})
                .catch(function(err) {
				   	reject(err);
			   	});
        },

        onAuthorize: function(data) {
        
            // Note: you can display a confirmation page before executing
            
			// Show a 'wait' cursor
			$('*').css('cursor', 'wait');

            var EXECUTE_PAYMENT_URL = serverUrl + "executepayment";

            paypal.request.post(EXECUTE_PAYMENT_URL,
                    { paymentID: data.paymentID, payerID: data.payerID })
                    
                .then(function(data) { 

					var newLocation = "ppaccept.html?number=" + data.number + "&ref=" + data.ref;
					if ( data.message !== undefined ) {
						newLocation = newLocation + "&message=" + data.message;
					} else {
						window.location.replace(newLocation);
					}
				})
                .catch(function(err) {

					var newLocation = "ppdecline.html?number=" + err.number + "&ref=" + err.ref;
					if ( err.message !== undefined ) {
						newLocation = newLocation + "&message=" + err.message;
					} else {
						window.location.replace(newLocation);
					}
			   	});
        },

		onCancel: function(data, actions) {

			return actions.redirect();

		}


    }, '#PayPalButton');
}
