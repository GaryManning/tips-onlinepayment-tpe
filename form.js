// Define some global variables
var serverUrl = "https://apps1.raspberrysoftware.com/tpe/tips/";

var currentStep = 0;
var maxSteps = 3;
var maxHeight = 0;

var digitsRequired = 9;
var digitNotice = digitsRequired + " characters required";

//var confirmMessage = ['Unpaid Fare Notice', 'Unpaid Fare Notice', 'Unpaid Fare Notice', 'Unpaid Fare Notice', 'Unpaid Fare Notice', 'Bylaw Report', 'Witness Statement'];
var confirmMessage = ['Notice', 'Notice', 'Notice', 'Notice', 'Notice', 'Notice', 'Notice'];
var flipoverMessage = "This Unpaid Fare Notice remains outstanding despite two reminder letters being sent, and is now being treated as a prosecution. However, you may still pay this Notice and stop the pending prosecution.";

function initialize() {

	// Once the page has loaded...
	// Add each page to the document and immediately hide it
	$.each(steps,
			 function (i, v) {
				$($.parseHTML(steps[i])).insertBefore('#messages').hide();
			}			
	);

	// Add a 'click' handler to the 'Next' button
	$('#nextButton').bind('click', nextButtonHandler);

	// Add a 'Cancel' button
	$('#nextButton').before($('<button id="cancelButton">No</button>'));
	// Add a 'click' handler to the 'Cancel' button
	$('#cancelButton').bind('click', cancelButtonHandler);

	// Add a 'Back' button
	$('#nextButton').before($('<button id="backButton">Back</button>').hide());

	// Add a 'Print button
	//$('#backButton').after($('<button id="printButton">Print</button>').hide());
	//$('#printButton').bind('click', function () { window.print(); });

	// Add 'keyup' (for input fields) and 'change' (for drop-downs)
	// handlers to the fields on the first form
	$('#form0 > :input').bind('keyup change', checkFieldValues).val("");

	// Add the 'currentStep' class to the current step in the breadcrumb div
	$('#breadcrumb ul').children().eq(currentStep).addClass('currentStep');

	// Get the maximum height of the largest 'step' div (in this case #step0)...
	maxHeight = $('#step1').css('height');

	// And set all the other heights to match it (so that the 'Next' button does not move
	$('#steps div:hidden').each(
			function (i, v) {
					$(v).css('height', maxHeight);
			});
	$('#step0').css('height', maxHeight);

	// Get the height of the buttons DIV - this will be used later, when the buttons
	// are removed from the last page, to prevent the page shrinking
	buttonsHeight = $('#buttons').css('height');

	// Set the initial input fields 
	resetInputFields();

	// Add the version number to the footer
	//$('#version').text("v" + Revision);

	//var page = location.search.split('page=')[1]
	var page = getUrlParam.page;
	if ( page ) {
		gotoNextPage(page);
	}

	// Set the digit notice field
	$('#digitnotice').text(digitNotice);
}

function resetInputFields() {
	// Reset the 'ticket number' and 'name' fields to empty and set the focus to the first one
	$('#form0 > :input').each(
			function (i,v) {
				$(v).val("");
				if ( i === 0 ) { $(v).focus(); }
				}
		);
	$('#nextButton').attr('disabled', true);
	$('#nextButton').text("Next");
	$('#cancelButton').hide();
	$('#cancelButton').text("No");

	setFormData();	// Used for testing
}

function getPenaltyData() {
		// Remove leading/trailing spaces from the input fields
		$('#form0 :input').each(function(i,e) { $(e).val($(e).val().replace(/^\s+|\s+$/g,''));})
		// Serialise the form data and submit it to the server for processing
		var formFields = $('#form0').serialize();
		$('#messages').removeClass('errorText').addClass('infoText').text("Checking...");
		return $.ajax({
			dataType: 'jsonp',
			data: formFields,
			url: serverUrl + "getpenaltydata"
		});
}

function initiatePayment(id) {
	// Initiate the payment process at the server and recieve some data
	return $.ajax({
		dataType: 'jsonp',
		data: {"id": id},
		url: serverUrl + "initiatepayment"
	});
}

function cancelButtonHandler() {
	location.search = "";
	resetInputFields();
	gotoNextPage(0);
}

function backButtonHandler() {
	location.search = "";
	resetInputFields();
	gotoNextPage(0);
}

function nextButtonHandler() {
	switch ( currentStep ) {
		case 0: 
			// While the current step is still the first, call 'getPenaltyData'
			$('*').css('cursor', 'wait');
			$('#nextButton').attr('disabled', true);
			$.when(getPenaltyData()).then( function (penaltyData) {
					$('*').css('cursor', 'normal');
					// Upon it's return, check that it finished ok
					if ( !penaltyData.found ) {
						// If penaltyData.found is not 'true' then it could be because the record was not found...
						if ( penaltyData.error ) {
							// Display the message (returned from the server) to the user
							$("#messages").removeClass('infoText').addClass('errorText').text(penaltyData.error);
						} else {
							// ... or because the call failed altogether (network failure?)
							// That's pretty serious, so inform the user
							alert("Error during lookup. Please try again, but if the error persists then please telephone the number below.");
						}

						// A record was not found so disable the 'Next' button and reset the input fields
						$('#nextButton').attr('disabled', true);
						resetInputFields();
						return;
					}

					if ( !penaltyData.cancollect ) {
						// For some reason the user is not allowed to pay for this ticket, so report the status and reset the fields
						$("#messages").removeClass('infoText').addClass('errorText').text("You are not able to make a payment for this Notice. The reason for this is that the Notice may already have been paid in full or the Notice has been closed.");
						$('#nextButton').attr('disabled', true);
						resetInputFields();
						return;
					}

					// Get all the input fields within the next form ('Notice Number', 'Name' etc)
					// and fill them with the values returned from the server call
					$('#form1 :input').each(
							function (i, v) {
								var field = $(v).attr('name');
								$(v).val(penaltyData[field]);
						 	});
					// Do the same with the payment form
					$('#form2 > :input').each(
							function (i, v) {
								var field = $(v).attr('name');
								if ( typeof(penaltyData[field]) === "number" ) {
									penaltyData[field] = penaltyData[field].toFixed(2);
								}
								$(v).val(penaltyData[field]);
							});
					// If the 'admin' value is 0, then remove the field from the screen to save confusing the user
					if ( penaltyData['admin'] == 0 ) {
						$('#form2 > :input[name="admin"]').hide().prev().hide().prev().hide();
						// Once the field has been removed the height of the div shrinks, so we need to set it back to
						// it's original height (saved in the initialise() function earlier)
						$('#step1').css('height', maxHeight);
					}
					// If it is a prosecution then remove all fields except the 'Amount owed' field
					if ( penaltyData['noticetype'] == 4 || penaltyData['noticetype'] == 5 || penaltyData['noticetype'] == 6 ) {
						$('#form2 > :input[name="admin"]').hide().prev().hide().prev().hide().prev().hide().prev().hide();
						// Once the field has been removed the height of the div shrinks, so we need to set it back to
						// it's original height (saved in the initialise() function earlier)
						$('#step1').css('height', maxHeight);
					}


					// Check notice type
					if ( penaltyData['noticetype'] == 0 || penaltyData['noticetype'] == 1 || penaltyData['noticetype'] == 2 ) {	// We shouldn't really get any of these
						$('#confirmText').text(confirmMessage[0]);
					}
					if ( penaltyData['noticetype'] == 3 ) {	// Notice
						$('#confirmText').text(confirmMessage[3]);
						$('#confirmAt').hide();
					}
					if ( penaltyData['noticetype'] == 4 ) {	// Flip-over
						$('#confirmText').parent().hide();
						$('#flipOver').text(flipoverMessage);
						$('#confirmFrom').hide();
						$('#confirmTo').hide();
					}
					if ( penaltyData['noticetype'] == 5 ) {	// Bylaw
						$('#confirmText').text(confirmMessage[5]);
						$('#confirmFrom').hide();
						$('#confirmTo').hide();
					}
					if ( penaltyData['noticetype'] == 6 ) {	// MG11
						$('#confirmText').text(confirmMessage[6]);
						$('#confirmFrom').remove();
						$('#confirmTo').remove();
					}

					// Set up the PayPal button (couldn't do it earlier as we need the guid from the first server call)
					setupPayPalButton(serverUrl, penaltyData['id']);

					// Reset the 'messages' area
					$('#messages').removeClass('errorText').addClass('infoText').text("");
					// Call the 'next page' function (incrementing by 1)
					$('#cancelButton').show();
					$('#nextButton').text("Confirm").attr('disabled', false);
					$('*').css('cursor', 'normal');
					gotoNextPage();
				});
			break;
		case 1: 
			$('#nextButton').text("Next");
			$('#cancelButton').text("Cancel");
			if ( $('#owed').val() == "0" ) {
				$('#nextButton').attr('disabled', true);
			}

			gotoNextPage();
			break;
		case 2:
			/*
			// Call the payment initiator
			$.when(initiatePayment($('#form2 :input#id').val())).then( function (paymentData) {
					$('#form3 > :input').each(
							function (i, v) {
								var field = $(v).attr('name');
								$(v).val(paymentData[field]);
								if ( field === "call" ) {
									$(v).val("submit");
								}
							});
						$('#form3').submit();
						});
			$.when(initiatePayment($('#form2 :input#id').val()))
				.then( function (redirect) {
						window.location.replace(redirect.url);
						});
			*/
			break;
		default:
			gotoNextPage();
	}
}

function gotoNextPage(page) {
	// Hide the current page
	$('#step'+currentStep).hide();

	// Remove the class attached to the current step in the breadcrumb bar...
	$('#breadcrumb ul').children().eq(currentStep).removeClass('currentStep');

	// Bind the current step to the back button so that we can return here after moving away
	if ( !$('#step'+currentStep).hasClass('info') ) {
		$('#backButton').unbind('click');
	}
	$('#backButton').bind('click', {'prevStep': currentStep}, function (e) {
		gotoNextPage(e.data.prevStep);
	});

	// Increment the page count
	if ( page !== undefined ) {
		currentStep = page;
	} else {
		currentStep += 1;
		page = 0;	// Force 'page' to ba a number so that it is not 'undefined' further down the script
	}

	// Show the new current page
	$('#step'+currentStep).show();

	// Add the current step class to the new current step in the breadcrumb trail
	$('#breadcrumb ul li').eq(currentStep).addClass('currentStep');
		
	if ( currentStep >= maxSteps ) {
		// Hide the 'Next' and 'Cancel' buttons if this is the last page
		$('#nextButton').hide();
		$('#cancelButton').text("Restart");
		$('#cancelButton').show();
		$('#backButton').hide();
		// Reset the height of the DIV to stop the page bottom jumping up
		$('#buttons').css('height', buttonsHeight);
		// Highlight the last li item on the breadcrumb
		$('#breadcrumb ul li:last').addClass('currentStep');
	} else {
		$('#nextButton').show();
		if ( currentStep !== 0 ) {	
			$('#cancelButton').text("Restart");
			$('#cancelButton').show();
		}
		if ( currentStep === 2 ) {	
			// PayPal button screen
			$('#nextButton').hide();
			$('#PayPalButton').css('display', 'inline-block').show();
		}
		$('#backButton').hide();
	}

	if ( typeof page !== 'number' ) {
		$('#nextButton').hide();
		$('#backButton').show();
		$('#breadcrumb ul li').removeClass('currentStep');
	}

	if ( $('#step'+currentStep).hasClass('info') ) {
		// Show the 'cancel' button but hide the 'back' one
		//$('#cancelButton').text("Back");
		//$('#cancelButton').show();
		//$('#backButton').hide();
		$('#cancelButton').hide();
		$('#backButton').show();
		$('#printButton').hide();
	}

	if ( $('#step'+currentStep).hasClass('final') ) {
		// Highlight the last li item on the breadcrumb
		$('#cancelButton').show();
		$('#backButton').hide();
		$('#breadcrumb ul li:last').addClass('currentStep');
		$('#printButton').show();
	}

}

function checkFieldValues(e) {
	if ( e.keyCode === 13 ) {
		// submit the form if the 'Next' button is enabled
		if ( !$('#nextButton').attr('disabled') ) { 
			nextButtonHandler();
		}
	}
	// Enable the 'Next' button
	$('#nextButton').attr('disabled', false);
 	$('#digitnotice').show();
	// Loop through all the input fields
	$('#form0 > :input').each(
			function (i,v) {
				if ( $(v).attr('name') === "number" ) {
					// The number field, check that there are 15 characters
					if ( $(v).val().length === 15 ) {
						$('#digitnotice').hide();
					}
				}
				if ( i < 2 ) {
					// For some reason ie8 returns 4 input fields, the last two of which are always empty
					// so to work around that we'll only check the first 2 (which should be all that are
					// returned by all browsers)
					if ( $(v).val() === "" || $(v).val() === null ) {
						// If any of them are empty then disable the 'Next' button again
						$('#nextButton').attr('disabled', true);
					}
				}
			}
 	);
}

function getPage(page) {
	$('#step'+page).load(page+'.html');
	gotoNextPage(page);
}

function setFormData () {
	return;
}
